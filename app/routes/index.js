const express = require('express');
const downloadAndConvert = require('../utils/downloadAndConvert');
const router = express.Router();

const images = [];

/** GET home page. */
router.get('/', (req, res, next) => {
  res.render('index', { title: 'Youtube video to Gif converter', images });
});

/** POST request */
router.post('/', async (req, res) => {
  const {url, start, duration} = req.body;

  try {
    const fileName = await downloadAndConvert(url,start, duration);
    const gif = fileName + '.gif';
    images.push(gif);
    res.json({ok: true, name: gif});
    
  } catch (error) {
    console.log(error);
    res.json({ok: false});
  }
});

module.exports = router;
